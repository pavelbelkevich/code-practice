package com.netcracker.edu.belkevich.code.entity;

import java.util.List;
import java.util.function.Predicate;

public interface Information {

    List<User> getUsersFilteredByPredicate(Predicate<User> predicate,List<User> users);

}
