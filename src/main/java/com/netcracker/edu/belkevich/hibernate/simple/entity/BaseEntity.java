package com.netcracker.edu.belkevich.hibernate.simple.entity;

import java.io.Serializable;

public abstract class BaseEntity implements Serializable, Cloneable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
            "id=" + id +
            '}';
    }
}
