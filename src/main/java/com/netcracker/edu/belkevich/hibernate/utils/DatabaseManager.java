package com.netcracker.edu.belkevich.hibernate.utils;

import javax.persistence.EntityManager;

public interface DatabaseManager {
   EntityManager getEntityManager();
}
